package no.eriksendesign.lwm_locationweatherandmap.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import no.eriksendesign.lwm_locationweatherandmap.data.LocationContract.LocationEntry;

/**
 * Created by dagro on 18.09.2016.
 */
public class LocationDbHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = LocationDbHelper.class.getSimpleName();

    /** Name of the database file */
    private static final String DATABASE_NAME = "locations.db";

    /**
     * Database version. If you change the database schema, you must increment the database version.
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Constructs a new instance of {@link LocationDbHelper}.
     *
     * @param context of the app
     */
    public LocationDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * This is called when the database is created for the first time.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create a String that contains the SQL statement to create the pets table
        String SQL_CREATE_LOCATIONS_TABLE =  "CREATE TABLE " + LocationEntry.TABLE_NAME + " ("
                + LocationEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + LocationEntry.COLUMN_LOCATION_NAME + " TEXT NOT NULL, "
                + LocationEntry.COLUMN_LOCATION_LATITUDE + " REAL NOT NULL, "
                + LocationEntry.COLUMN_LOCATION_LONGITUDE + " REAL NOT NULL); ";

        // Execute the SQL statement
        db.execSQL(SQL_CREATE_LOCATIONS_TABLE);
    }

    /**
     * This is called when the database needs to be upgraded.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // The database is still at version 1, so there's nothing to do be done here.
    }

}
