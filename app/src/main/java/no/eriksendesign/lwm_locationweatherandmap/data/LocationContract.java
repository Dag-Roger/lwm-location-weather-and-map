package no.eriksendesign.lwm_locationweatherandmap.data;

import android.provider.BaseColumns;

/**
 * Created by dagro on 18.09.2016.
 */
public final class LocationContract {
    private LocationContract() {}

    public static final class LocationEntry implements BaseColumns {

        /** Name of database table for locations */
        public final static String TABLE_NAME = "locations";

        /**
         * Unique ID number for the pet (only for use in the database table).
         *
         * Type: INTEGER
         */
        public final static String _ID = BaseColumns._ID;

        /**
         * Name of the location.
         *
         * Type: TEXT
         */
        public final static String COLUMN_LOCATION_NAME ="name";

        /**
         * Latitude.
         *
         * Type: REAL
         */
        public final static String COLUMN_LOCATION_LATITUDE ="latitude";

        /**
         * Longitude.
         *
         * Type: REAL
         */
        public final static String COLUMN_LOCATION_LONGITUDE ="longitude";
    }
}
