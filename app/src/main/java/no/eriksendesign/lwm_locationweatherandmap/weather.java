package no.eriksendesign.lwm_locationweatherandmap;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

import no.eriksendesign.lwm_locationweatherandmap.model.Weather;

public class weather extends AppCompatActivity {

    private TextView cityText;
    private TextView condDescr;
    private TextView temp;
    private TextView maxTemp;
    private TextView minTemp;
    private ImageView imgView;

    private double weatherLat;
    private double weatherLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

//        Get the latitude and longitude
        Intent intent = getIntent();
        weatherLat = intent.getDoubleExtra(MainActivity.EXTRA_LATITUDE, 0.00);
        weatherLong = intent.getDoubleExtra(MainActivity.EXTRA_LONGITUDE, 0.00);

        String city = "lat=" + weatherLat + "&lon=" + weatherLong;

        cityText = (TextView) findViewById(R.id.cityText);
        condDescr = (TextView) findViewById(R.id.condDescr);
        temp = (TextView) findViewById(R.id.temp);
        maxTemp = (TextView) findViewById(R.id.maxTemp);
        minTemp = (TextView) findViewById(R.id.minTemp);

//        imgView = (ImageView) findViewById(R.id.condIcon);

        JSONWeatherTask task = new JSONWeatherTask();
        task.execute(new String[]{city});
    }


    private class JSONWeatherTask extends AsyncTask<String, Void, Weather> {

        @Override
        protected Weather doInBackground(String... params) {
            Weather weather = new Weather();
            String data = ( (new WeatherHttpClient()).getWeatherData(params[0]));

            try {
                weather = JSONWeatherParser.getWeather(data);

                // Let's retrieve the icon
                weather.iconData = ( (new WeatherHttpClient()).getImage(weather.currentCondition.getIcon()));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return weather;

        }

        @Override
        protected void onPostExecute(Weather weather) {
            super.onPostExecute(weather);

//            if (weather.iconData != null && weather.iconData.length > 0) {
//                Bitmap img = BitmapFactory.decodeByteArray(weather.iconData, 0, weather.iconData.length);
//                imgView.setImageBitmap(img);
//            }

//            imgView.setImageURI(Uri.parse("http://openweathermap.org/img/w/" + weather.currentCondition.getIcon() + ".png"));

            cityText.setText(weather.location.getCity() + "," + weather.location.getCountry());
            condDescr.setText(weather.currentCondition.getCondition() + "(" + weather.currentCondition.getDescr() + ")");
            temp.setText("" + Math.round((weather.temperature.getTemp() - 273.15)) + "°C");
            maxTemp.setText(Math.round((weather.temperature.getMaxTemp() - 273.15)) + "°C");
            minTemp.setText(Math.round((weather.temperature.getMinTemp() - 273.15)) + "°C");

        }
    }
}
